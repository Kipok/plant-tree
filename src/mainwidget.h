#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include "tree.h"

#include <QGLWidget>
#include <QGLFunctions>
#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector2D>
#include <QBasicTimer>
#include <QGLShaderProgram>

class MainWidget : public QGLWidget, protected QGLFunctions
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent = 0);
    virtual ~MainWidget();

protected:
    virtual void mousePressEvent(QMouseEvent *e) override;
    virtual void mouseReleaseEvent(QMouseEvent *) override;
    virtual void timerEvent(QTimerEvent *e) override;
    virtual void keyPressEvent(QKeyEvent *e) override;
    virtual void keyReleaseEvent(QKeyEvent *e) override;
    virtual void mouseMoveEvent(QMouseEvent *e) override;

    virtual void initializeGL() override;
    virtual void resizeGL(int w, int h) override;
    virtual void paintGL() override;


    void initShaders();
    void initTextures();

private:
    QBasicTimer timer;
    QGLShaderProgram program;

    GLuint texture;
    int useTexture;

    QMatrix4x4 projectionMatrix;
    std::unique_ptr<Tree> tree;

    QVector2D mousePressPosition;
    QVector3D rotationAxis;
    double cameraMove;
    QVector3D lookPoint;
    QVector3D cameraPosition;
    QVector3D upVector;
    uint time;
    int s_time;
    qreal zNear, zFar, fov, fov_update;
    bool key_pressed;
    bool mouse_pressed;
    double swing_angle;
    double swing_update;
};

#endif // MAINWIDGET_H
