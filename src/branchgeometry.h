#ifndef BRANCHGEOMETRY_H
#define BRANCHGEOMETRY_H

#include "basicgeometry.h"

class BranchGeometry : public BasicGeometry
{
    virtual void initGeometry() override;
public:
    BranchGeometry(QGLWidget *context);
    virtual ~BranchGeometry();
};

#endif // BRANCHGEOMETRY_H
