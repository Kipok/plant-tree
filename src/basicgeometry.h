#ifndef BASICGEOMETRY_H
#define BASICGEOMETRY_H

#include <QGLFunctions>
#include <QGLShaderProgram>
#include <QVector2D>
#include <QVector3D>
#include <QGLWidget>
#include <vector>

#define pi 3.1415926535897

class BasicGeometry : protected QGLFunctions
{
    virtual void initGeometry() = 0;
public:
    BasicGeometry(QGLWidget *context, const QImage &texture, int isLeaf);
    virtual ~BasicGeometry();

    struct VertexData
    {
        QVector3D position;
        QVector2D texCoord;
        QVector3D normal;
        VertexData(QVector3D p) : position(p), texCoord(0.0f, 0.0f), normal(0.0f, 0.0f, 0.0f) {
            texCoord = QVector2D(p.x() - 0.5, p.y() - 1.0);
        }
        VertexData(QVector3D p, QVector3D n) : position(p), texCoord(0.0f, 0.0f), normal(n) {
            texCoord = QVector2D(p.x()/4.0 - 0.5, p.y()/12.0 - 1.0);
        }
        VertexData() {}
    };

    QVector3D computeNormal(int i);
    void fillNormals();
    QGLWidget *context;
    QImage texture;
    int isLeaf;

    std::vector <GLushort> indices;
    std::vector <VertexData> vertices;
    GLuint vboIds[2];
    void init();
    void drawGeometry(QGLShaderProgram *program);
};

#endif // BASICGEOMETRY_H
