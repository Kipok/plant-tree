#include "branchgeometry.h"

BranchGeometry::BranchGeometry(QGLWidget *context) : BasicGeometry(context, QImage(":/tree.png"), 0)
{
}

BranchGeometry::~BranchGeometry()
{
}

void BranchGeometry::initGeometry()
{
    vertices.clear();
    indices.clear();
    int num_radial = 30;
    double cone_coef = sqrt(0.3);
    vertices.push_back(VertexData(QVector3D(0.0f, 0.0f, 0.0f)));
    for (int i = 0; i < num_radial; i++) {
        double angle = 2*pi * i / num_radial;
        vertices.push_back(VertexData(QVector3D(cos(angle), 0.0f, sin(angle))));
    }
    vertices.push_back(VertexData(QVector3D(0.0f, 1.0f, 0.0f)));
    for (int i = 0; i < num_radial; i++) {
        double angle = 2*pi * i / num_radial;
        vertices.push_back(VertexData(QVector3D(cone_coef*cos(angle), 1.0f, cone_coef*sin(angle))));
    }
    // first circle
    for (int i = 1; i <= num_radial; i++) {
        indices.push_back(0);
        indices.push_back(i);
        indices.push_back(i != num_radial ? i + 1 : 1);
    }
    // second circle
    for (int i = num_radial + 2; i <= 2 * num_radial + 1; i++) {
        indices.push_back(num_radial + 1);
        indices.push_back(i != 2 * num_radial + 1 ? i + 1 : num_radial + 2);
        indices.push_back(i);
    }
    //side surface
    for (int i = 1; i <= num_radial; i++) {
        indices.push_back(i + 0);
        indices.push_back(num_radial + 1 + (i != num_radial ? i + 1 : 1));
        indices.push_back(i != num_radial ? i + 1 : 1);

        indices.push_back(num_radial + 1 + (i != num_radial ? i + 1 : 1));
        indices.push_back(i + 0);
        indices.push_back(i + num_radial + 1);
    }
    fillNormals();

    // Transfer vertex data to VBO 0
    glBindBuffer(GL_ARRAY_BUFFER, vboIds[0]);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(VertexData), &vertices[0], GL_STATIC_DRAW);

    // Transfer index data to VBO 1
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIds[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLushort), &indices[0], GL_STATIC_DRAW);
}

