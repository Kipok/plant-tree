#version 330 core
#ifdef GL_ES
// Set default precision to medium
precision mediump int;
precision mediump float;
#endif

uniform sampler2D texture;
uniform int useTexture;
uniform int season;
uniform float summer_coef;
uniform float autumn_coef;
uniform float winter_coef;
uniform int isLeaf;

in vec3 fragmentNormal;
in vec3 fragmentEye;
in vec3 lightLocationEye;
in vec2 texcoord;

const vec4 ka= vec4(0.05, 0.05, 0.05, 1);
const vec4 ks = vec4(0.4, 0.4, 0.4, 0.4);
const float ke = 20;

void main()
{
    vec4 materialKd;
    // Material properties
    if (useTexture > 0) {
        if (season == 1) {
            materialKd = texture2D(texture, texcoord).rgba;
        }
        if (season == 2) {
            materialKd = texture2D(texture, texcoord).rgba * summer_coef;
        }
        if (season == 3) {
            materialKd = texture2D(texture, texcoord).rgba * summer_coef;
            if (isLeaf > 0) {
                materialKd = vec4(materialKd.r, materialKd.g * autumn_coef, materialKd.b * autumn_coef, materialKd.a);
            }
        }
        if (season == 4) {
            materialKd = texture2D(texture, texcoord).rgba;
            if (isLeaf > 0) {
                materialKd = vec4(materialKd.r, materialKd.g * autumn_coef, materialKd.b * autumn_coef, materialKd.a);
            } else {
                materialKd = vec4(materialKd.r * winter_coef, materialKd.bga);
            }
        }
    } else {
        materialKd = vec4(0.5,0.2,0.1,1.0);
    }

    vec3 light = normalize(lightLocationEye - fragmentEye);
    vec3 normal = normalize(fragmentNormal);
    vec3 eye = normalize(fragmentEye);
    float diffuseIntensity = clamp(max(dot(normal, light), 0.0), 0.0, 1.0);
    vec3 reflection = normalize(reflect(light, normal));
    float specularIntensity = pow(clamp(max(dot(reflection, eye), 0.0), 0.0, 1.0), ke);
    gl_FragColor = ka + materialKd * diffuseIntensity + ks * specularIntensity;
 //   gl_FragColor = materialKd;
}
