#include "basicgeometry.h"

#include <algorithm>
#include <numeric>

BasicGeometry::BasicGeometry(QGLWidget *context, const QImage &texture, int isLeaf)
{
    this->context = context;
    this->texture = texture;
    this->isLeaf = isLeaf;
}

BasicGeometry::~BasicGeometry()
{
    glDeleteBuffers(2, vboIds);
}

void BasicGeometry::init()
{
    initializeGLFunctions();

    // Generate 2 VBOs
    glGenBuffers(2, vboIds);

    // Initializes geometries and transfers them to VBOs
    initGeometry();
}

QVector3D BasicGeometry::computeNormal(int i)
{
    int v1 = indices[3*i + 0];
    int v2 = indices[3*i + 1];
    int v3 = indices[3*i + 2];
    auto tmp1 = (vertices[v2].position - vertices[v1].position);
    auto tmp2 = (vertices[v3].position - vertices[v2].position);
    auto tmp3 = QVector3D::crossProduct(tmp1, tmp2);
    double sin_alpha = tmp3.length() / (tmp1.length() * tmp2.length());
    return tmp3.normalized() * asin(sin_alpha);
}

void BasicGeometry::fillNormals()
{
    std::vector<std::vector<int>> vert_triangles(vertices.size());
    for (int i = 0; i < indices.size(); i++) {
        vert_triangles[indices[i]].push_back(i / 3);
    }
    std::vector<QVector3D> normals;
    for (int i = 0; i < vert_triangles.size(); i++) {
        normals.clear();
        for (int j = 0; j < vert_triangles[i].size(); j++) {
            normals.push_back(computeNormal(vert_triangles[i][j]));
        }
        if (i == 3) {
            i = i;
        }
        const double eps = 1e-4;
        sort(normals.begin(), normals.end(), [eps](const QVector3D &v1, const QVector3D &v2) {
            if (!(fabs(v1.x() - v2.x()) < eps) && v1.x() < v2.x()) {
                return true;
            } else if (!(fabs(v1.x() - v2.x()) < eps) && v1.x() > v2.x()) {
                return false;
            }
            if (!(fabs(v1.y() - v2.y()) < eps) && v1.y() < v2.y()) {
                return true;
            } else if (!(fabs(v1.y() - v2.y()) < eps) && v1.y() > v2.y()) {
                return false;
            }
            if (!(fabs(v1.z() - v2.z()) < eps) && v1.z() < v2.z()) {
                return true;
            } else if (!(fabs(v1.z() - v2.z()) < eps) && v1.z() > v2.z()) {
                return false;
            }
            return false;
        }); // TODO: change to correct double comparizon!
        normals.erase(unique(normals.begin(), normals.end(),
                             [eps](const QVector3D &v1, const QVector3D &v2) {
            return fabs(v1.x() - v2.x()) < eps &&
                   fabs(v1.y() - v2.y()) < eps &&
                   fabs(v1.z() - v2.z()) < eps;
        }), normals.end());

        vertices[i].normal = std::accumulate(normals.begin(),normals.end(),QVector3D(0.0, 0.0, 0.0));
        vertices[i].normal.normalize();
    }
}

void BasicGeometry::drawGeometry(QGLShaderProgram *program)
{
    // Tell OpenGL which VBOs to use
    glBindBuffer(GL_ARRAY_BUFFER, vboIds[0]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIds[1]);

    context->bindTexture(texture);
    program->setUniformValue("isLeaf", isLeaf);

    // Offset for position
    quintptr offset = 0;

    // Tell OpenGL programmable pipeline how to locate vertex position data
    int vertexLocation = program->attributeLocation("a_position");
    program->enableAttributeArray(vertexLocation);
    glVertexAttribPointer(vertexLocation, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    // Offset for texture coordinate
    offset += sizeof(QVector3D);

    // Tell OpenGL programmable pipeline how to locate vertex texture coordinate data
    int texcoordLocation = program->attributeLocation("a_texcoord");
    program->enableAttributeArray(texcoordLocation);
    glVertexAttribPointer(texcoordLocation, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    offset += sizeof(QVector2D);

    // Tell OpenGL programmable pipeline how to locate vertex normal data
    int normalLocation = program->attributeLocation("a_normal");
    program->enableAttributeArray(normalLocation);
    glVertexAttribPointer(normalLocation, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)offset);

    // Draw branch geometry using indices from VBO 1
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, 0);
}
