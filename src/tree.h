#ifndef TREE_H
#define TREE_H

#include "treenode.h"
#include "branchgeometry.h"
#include "leafgeometry.h"

class Tree
{
    std::unique_ptr<TreeNode> root;
    std::shared_ptr<BranchGeometry> branchGeom;
    std::shared_ptr<LeafGeometry> leafGeom;
    double update_rate;
    int plant_mod;
public:
    Tree(QGLWidget *context);
    void draw(QGLShaderProgram *program, const QMatrix4x4 &projectionMatrix,
              const QMatrix4x4 &viewMatrix, const QMatrix4x4 &modelMatrix);
    void init_geometries();
    void grow();
    double get_growth() const {
        return root->get_growth();
    }

    void updateRate(double x) {
        update_rate *= x;
    }
    void updatePlant(double x) {
        plant_mod /= x;
        if (plant_mod == 0) {
            plant_mod = 1;
        }
    }
    int use_update;
    int drop;
    int mode;
};

#endif // TREE_H
