#include "mainwidget.h"

#include <QMouseEvent>

#include <math.h>

MainWidget::MainWidget(QWidget *parent) :
    QGLWidget(parent),
    zNear(0.01),
    zFar(40.0),
    fov(45.0),
    fov_update(0.0),
    key_pressed(false),
    lookPoint(0.0f, 1.0f, 0.0f),
    cameraPosition(0.0f, 1.0f, 12.0f),
    upVector(0.0f, 1.0f, 0.0f),
    mouse_pressed(false),
    mousePressPosition(0.0f, 0.0f),
    cameraMove(0.0),
    swing_angle(0.0),
    swing_update(0.03),
    time(0),
    s_time(1000)
{
}

MainWidget::~MainWidget()
{
    deleteTexture(texture);
}

void MainWidget::mousePressEvent(QMouseEvent *)
{
    mouse_pressed = true;
    QPoint glob = mapToGlobal(QPoint(width()/2,height()/2));
    QCursor::setPos(glob);
}

void MainWidget::mouseReleaseEvent(QMouseEvent *)
{
    mouse_pressed = false;
}

void MainWidget::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Space) {
        useTexture = (useTexture + 1) % 2;
        updateGL();
    }
    if (e->key() == Qt::Key_Escape) {
        lookPoint = QVector3D(0.0f, 1.0f, 0.0f);
        cameraPosition = QVector3D(0.0f, 1.0f, 12.0f);
        cameraMove = 0.0;
        upVector = QVector3D(0.0f, 1.0f, 0.0f);
        mouse_pressed = false;
        key_pressed = false;
        mousePressPosition = QVector2D(0.0,0.0);
    }
    if (e->key() == Qt::Key_A) {
        key_pressed = true;
        rotationAxis = -upVector;
        cameraMove = 0.0;
    }
    if (e->key() == Qt::Key_D) {
        key_pressed = true;
        rotationAxis = upVector;
        cameraMove = 0.0;
    }
    if (e->key() == Qt::Key_W) {
        key_pressed = true;
        rotationAxis = -QVector3D::crossProduct(upVector, cameraPosition - lookPoint);
        cameraMove = 0.0;
    }
    if (e->key() == Qt::Key_S) {
        key_pressed = true;
        rotationAxis = QVector3D::crossProduct(upVector, cameraPosition - lookPoint);
        cameraMove = 0.0;
    }
    if (e->key() == Qt::Key_Q) {
        key_pressed = true;
        rotationAxis = QVector3D(0.0, 0.0, 0.0);
        cameraMove = -0.05;
    }
    if (e->key() == Qt::Key_E) {
        key_pressed = true;
        rotationAxis = QVector3D(0.0, 0.0, 0.0);
        cameraMove = 0.05;
    }
    if (e->key() == Qt::Key_Left) {
        tree->updateRate(0.8);
        tree->updatePlant(0.8);
        s_time /= 0.8;
        if (s_time > 100000) {
            s_time = 100000;
        }
    }
    if (e->key() == Qt::Key_Right) {
        tree->updateRate(1/0.8);
        tree->updatePlant(1/0.8);
        s_time *= 0.8;
        if (s_time < 50) {
            s_time = 50;
        }
    }
}

void MainWidget::keyReleaseEvent(QKeyEvent *)
{
    cameraMove = 0.0;
    rotationAxis = QVector3D(0.0, 0.0, 0.0);
}

void MainWidget::mouseMoveEvent(QMouseEvent *e)
{
    if (mouse_pressed) {
        QVector2D dir = QVector2D(e->localPos()) - QVector2D(width()/2.0, height()/2.0);
        lookPoint += 0.005*QVector3D(dir.x(), -dir.y(), 0.0f);
        QVector3D newRight = QVector3D::crossProduct(upVector, cameraPosition - lookPoint).normalized();
        upVector = -QVector3D::crossProduct(newRight, cameraPosition - lookPoint).normalized();
        QPoint glob = mapToGlobal(QPoint(width()/2,height()/2));
        QCursor::setPos(glob);
    }
}

void MainWidget::timerEvent(QTimerEvent *)
{
    tree->grow();
    updateGL();
}

void MainWidget::initializeGL()
{
    initializeGLFunctions();
    qglClearColor(Qt::black);
    initShaders();
    initTextures();

    // Enable depth buffer
    glEnable(GL_DEPTH_TEST);

    // Enable back face culling
    glEnable(GL_CULL_FACE);

    tree = std::make_unique<Tree>(this);
    tree->init_geometries();

    // Use QBasicTimer because its faster than QTimer
    timer.start(12, this);
}

void MainWidget::initShaders()
{
    // Compile vertex shader
    if (!program.addShaderFromSourceFile(QGLShader::Vertex, ":/vshader.glsl")) {
        close();
    }
    // Compile fragment shader
    if (!program.addShaderFromSourceFile(QGLShader::Fragment, ":/fshader.glsl")) {
        close();
    }
    // Link shader pipeline
    if (!program.link()) {
        close();
    }
    // Bind shader pipeline for use
    if (!program.bind()) {
        close();
    }
}

void MainWidget::initTextures()
{
    useTexture = 1;
    // Load cube.png image
    glEnable(GL_TEXTURE_2D);

    // Set nearest filtering mode for texture minification
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    // Set bilinear filtering mode for texture magnification
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Wrap texture coordinates by repeating
    // f.ex. texture coordinate (1.1, 1.2) is same as (0.1, 0.2)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

void MainWidget::resizeGL(int w, int h)
{
    // Set OpenGL viewport to cover whole widget
    glViewport(0, 0, w, h);

    // Calculate aspect ratio
    qreal aspect = qreal(w) / qreal(h ? h : 1);

    // Reset projection
    projectionMatrix.setToIdentity();

    // Set perspective projection
    projectionMatrix.perspective(fov, aspect, zNear, zFar);
}

void MainWidget::paintGL()
{
    // Clear color and depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Calculate model view transformation
    QMatrix4x4 viewMatrix;
    QMatrix4x4 modelMatrix;

    QMatrix4x4 tmp;
    tmp.rotate(QQuaternion::fromAxisAndAngle(rotationAxis, 2.5));
    cameraPosition = tmp * cameraPosition;
    upVector = tmp * upVector;
    cameraPosition += cameraMove * (lookPoint - cameraPosition).normalized();

    viewMatrix.lookAt(cameraPosition, lookPoint, upVector);

    ++time;
    int season = 1;
    float summer_coef = 1.0;
    float autumn_coef = 1.0;
    float winter_coef = 1.0;
    if (time < s_time) {
        season = 1;
        tree->use_update = 1;
        tree->drop = 0;
    }
    if (s_time <= time && time < 2 * s_time) {
        season = 2;
        summer_coef = 1.0 - 0.5 * (time - s_time) / s_time;
        tree->use_update = 0;
        tree->drop = 0;
    }
    if (2 * s_time <= time && time < 3 * s_time) {
        season = 3;
        autumn_coef = 1.0 - 0.8 * (time - 2 * s_time) / s_time;
        summer_coef = 0.5 + 0.5 * (time - 2 * s_time) / s_time;
        tree->use_update = 0;
        tree->drop = 0;
    }
    if (3 * s_time <= time && time < 5 * s_time) {
        season = 4;
        autumn_coef = 0.2 + 0.8 * (time - 3 * s_time) / (2*s_time);
        winter_coef = 1.0 - 0.3 * (time - 3 * s_time) / (2*s_time);
        tree->use_update = 0;
        tree->drop = 1;
    }
    if (5 * s_time <= time) {
        season = 1;
        time = 0;
        tree->use_update = 1;
        tree->drop = 0;
        tree->mode = 1;
    }

    program.setUniformValue("season", season);
    program.setUniformValue("summer_coef", summer_coef);
    program.setUniformValue("autumn_coef", autumn_coef);
    program.setUniformValue("winter_coef", winter_coef);

    program.setUniformValue("texture", 0);

    program.setUniformValue("useTexture", useTexture);

    if (tree->get_growth() == 1.0) {
        if (swing_angle >= 1.0 || swing_angle <= -1.0) {
            swing_update *= -1.0;
        }
        swing_angle += swing_update;
    }
    modelMatrix.rotate(QQuaternion::fromAxisAndAngle(QVector3D(0.0, 0.0, -1.0), swing_angle));
    tree->draw(&program, projectionMatrix, viewMatrix, modelMatrix);
}
