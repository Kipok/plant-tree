#version 330 core
#ifdef GL_ES
// Set default precision to medium
precision mediump int;
precision mediump float;
#endif

uniform mat4 mvp_matrix;
uniform mat4 mv_matrix;
uniform mat4 v_matrix;
uniform mat3 normalMatrix;

attribute vec3 a_position;
attribute vec2 a_texcoord;
attribute vec3 a_normal;

out vec3 fragmentNormal;
out vec3 fragmentEye;
out vec3 lightLocationEye;
out vec2 texcoord;

const vec4 lightLocation = vec4(0.0, 100.0, 100.0, 1.0);

void main()
{
    // Calculate vertex position in screen space
    gl_Position = mvp_matrix * vec4(a_position, 1.0);
    texcoord = a_texcoord;

    fragmentEye = (mv_matrix * vec4(a_position, 1.0)).xyz;
    fragmentNormal = normalMatrix * a_normal;
    lightLocationEye = (v_matrix * lightLocation).xyz;
}
