#ifndef LEAFGEOMETRY_H
#define LEAFGEOMETRY_H

#include "basicgeometry.h"

class LeafGeometry : public BasicGeometry
{
    virtual void initGeometry() override;
public:
    LeafGeometry(QGLWidget *context);
    virtual ~LeafGeometry();
};

#endif // LEAFGEOMETRY_H
