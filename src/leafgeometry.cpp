#include "leafgeometry.h"

LeafGeometry::LeafGeometry(QGLWidget *context) : BasicGeometry(context, QImage(":/leaf.png"), 1)
{
}

LeafGeometry::~LeafGeometry()
{
}

void LeafGeometry::initGeometry()
{
    // initialize key points
    vertices.push_back(VertexData(QVector3D(0.0f, 0.0f, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
    vertices.push_back(VertexData(QVector3D(0.0f, 2.0f, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
    vertices.push_back(VertexData(QVector3D(0.0f, 3.5f, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
    vertices.push_back(VertexData(QVector3D(0.0f, 6.4f, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
    vertices.push_back(VertexData(QVector3D(0.0f, 12.0f, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
    vertices.push_back(VertexData(QVector3D(3.0f, 3.5f, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
    vertices.push_back(VertexData(QVector3D(3.0f, 6.5f, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
    vertices.push_back(VertexData(QVector3D(1.73f, 9.0f, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
    vertices.push_back(VertexData(QVector3D(-3.0f, 3.5f, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
    vertices.push_back(VertexData(QVector3D(-3.0f, 6.5f, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
    vertices.push_back(VertexData(QVector3D(-1.73f, 9.0f, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));

    int key_offset = vertices.size();

    vertices.push_back(VertexData(QVector3D(0.0f, 0.0f, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));
    vertices.push_back(VertexData(QVector3D(0.0f, 2.0f, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));
    vertices.push_back(VertexData(QVector3D(0.0f, 3.5f, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));
    vertices.push_back(VertexData(QVector3D(0.0f, 6.4f, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));
    vertices.push_back(VertexData(QVector3D(0.0f, 12.0f, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));
    vertices.push_back(VertexData(QVector3D(3.0f, 3.5f, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));
    vertices.push_back(VertexData(QVector3D(3.0f, 6.5f, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));
    vertices.push_back(VertexData(QVector3D(1.73f, 9.0f, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));
    vertices.push_back(VertexData(QVector3D(-3.0f, 3.5f, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));
    vertices.push_back(VertexData(QVector3D(-3.0f, 6.5f, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));
    vertices.push_back(VertexData(QVector3D(-1.73f, 9.0f, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));

    int offset = vertices.size();
    int accuracy = 30;

    for (int i = 0; i < accuracy; i++) {
        double x = (i + 1) * 4.0 / accuracy;
        double y = x * x / 4.0;
        vertices.push_back(VertexData(QVector3D(x, y, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
        vertices.push_back(VertexData(QVector3D(-x, y, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
        vertices.push_back(VertexData(QVector3D(x, y, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));
        vertices.push_back(VertexData(QVector3D(-x, y, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));
        indices.push_back(0);
        indices.push_back(offset + 4 * i);
        indices.push_back(1);
        indices.push_back(1);
        indices.push_back(offset + 4 * i + 1);
        indices.push_back(0);

        indices.push_back(1 + key_offset);
        indices.push_back(offset + 4 * i + 2);
        indices.push_back(0 + key_offset);
        indices.push_back(0 + key_offset);
        indices.push_back(offset + 4 * i + 3);
        indices.push_back(1 + key_offset);
    }
    indices.push_back(1);
    indices.push_back(5);
    indices.push_back(8);
    indices.push_back(8 + key_offset);
    indices.push_back(5 + key_offset);
    indices.push_back(1 + key_offset);

    offset = vertices.size();

    for (int i = 0; i < accuracy; i++) {
        double x = 3 + (i + 1) * 0.5 / accuracy;
        double y = x * x * 10.0 - 58.0 * x + 87.5;
        vertices.push_back(VertexData(QVector3D(x, y, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
        vertices.push_back(VertexData(QVector3D(-x, y, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
        vertices.push_back(VertexData(QVector3D(x, y, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));;
        vertices.push_back(VertexData(QVector3D(-x, y, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));;
        indices.push_back(5);
        indices.push_back(offset + 4 * i);
        indices.push_back(2);
        indices.push_back(2);
        indices.push_back(offset + 4 * i + 1);
        indices.push_back(8);

        indices.push_back(2 + key_offset);
        indices.push_back(offset + 4 * i + 2);
        indices.push_back(5 + key_offset);
        indices.push_back(8 + key_offset);
        indices.push_back(offset + 4 * i + 3);
        indices.push_back(2 + key_offset);
    }
    indices.push_back(2);
    indices.push_back(6);
    indices.push_back(9);
    indices.push_back(9 + key_offset);
    indices.push_back(6 + key_offset);
    indices.push_back(2 + key_offset);

    offset = vertices.size();

    for (int i = 0; i < accuracy; i++) {
        double x = 2 + (i + 1) * 1.0 / accuracy;
        double y = x * x * (-2.13904) + 7.69519 * x + 2.66578;
        vertices.push_back(VertexData(QVector3D(x, y, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
        vertices.push_back(VertexData(QVector3D(-x, y, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
        vertices.push_back(VertexData(QVector3D(x, y, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));;
        vertices.push_back(VertexData(QVector3D(-x, y, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));;
        indices.push_back(6);
        indices.push_back(offset + 4 * i);
        indices.push_back(3);
        indices.push_back(3);
        indices.push_back(offset + 4 * i + 1);
        indices.push_back(9);

        indices.push_back(3 + key_offset);
        indices.push_back(offset + 4 * i + 2);
        indices.push_back(6 + key_offset);
        indices.push_back(9 + key_offset);
        indices.push_back(offset + 4 * i + 3);
        indices.push_back(3 + key_offset);
    }
    indices.push_back(2);
    indices.push_back(6);
    indices.push_back(9);
    indices.push_back(9 + key_offset);
    indices.push_back(6 + key_offset);
    indices.push_back(2 + key_offset);

    offset = vertices.size();

    for (int i = 0; i < accuracy; i++) {
        double y = 9 + (i + 1) * 3.0 / accuracy;
        double x = sqrt(12.0 - y);
        vertices.push_back(VertexData(QVector3D(x, y, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
        vertices.push_back(VertexData(QVector3D(-x, y, 0.001f), QVector3D(0.0f, 0.0f, 1.0f)));
        vertices.push_back(VertexData(QVector3D(x, y, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));;
        vertices.push_back(VertexData(QVector3D(-x, y, -0.001f), QVector3D(0.0f, 0.0f, -1.0f)));;
        indices.push_back(7);
        indices.push_back(offset + 4 * i);
        indices.push_back(3);
        indices.push_back(3);
        indices.push_back(offset + 4 * i + 1);
        indices.push_back(10);

        indices.push_back(3 + key_offset);
        indices.push_back(offset + 4 * i + 2);
        indices.push_back(7 + key_offset);
        indices.push_back(10 + key_offset);
        indices.push_back(offset + 4 * i + 3);
        indices.push_back(3 + key_offset);
    }

    // Transfer vertex data to VBO 0
    glBindBuffer(GL_ARRAY_BUFFER, vboIds[0]);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(VertexData), &vertices[0], GL_STATIC_DRAW);

    // Transfer index data to VBO 1
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIds[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLushort), &indices[0], GL_STATIC_DRAW);

}
