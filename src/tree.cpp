#include "tree.h"
#include <ctime>

Tree::Tree(QGLWidget *context)
{
    srand(time(0));
    branchGeom = std::make_shared<BranchGeometry>(context);
    leafGeom = std::make_shared<LeafGeometry>(context);
    root = std::make_unique<TreeNode>(0, 0, 0, 1, branchGeom, QVector3D(0.1f, 5.0f, 0.1f), false, 0);
    root->setMaxBranches(40);
    update_rate = 0.001;
    use_update = 1;
    plant_mod = 50;
    drop = 0;
    mode = 0;
}

void Tree::draw(QGLShaderProgram *program, const QMatrix4x4 &projectionMatrix,
                const QMatrix4x4 &viewMatrix, const QMatrix4x4 &modelMatrix)
{
    root->drawMe(program, projectionMatrix, viewMatrix, modelMatrix, mode);
}

void Tree::init_geometries()
{
    branchGeom->init();
    leafGeom->init();
}

void Tree::grow()
{
    root->updateNode(update_rate * use_update, 0, leafGeom, plant_mod, drop);
}
