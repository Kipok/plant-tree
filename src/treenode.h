#ifndef TREENODE_H
#define TREENODE_H

#include <vector>
#include <memory>
#include <QMatrix4x4>
#include <QGLShaderProgram>

#include "basicgeometry.h"

class TreeNode
{
    std::vector <std::unique_ptr<TreeNode>> nodes;
    QVector3D real_scale;
    QVector3D scale;
    std::vector <bool> b_slots;

    int max_branches;
    const double min_length = 0.9;
    const double min_res_vert = 20;
    const double max_res_vert = 80;

    const double min_height = 0.3;
    const double max_height = 0.97;
    const double scale_min = 0.2;
    const double scale_max = 0.5;
    const double vert_min = 15.0;
    const double vert_max = 80.0;
    const double hor_min = 0.0;
    const double hor_max = 360;
    const int hor_range = 10;

    double pos;
    double hor_angle;
    double vert_angle;
    double cur_scale;
    double growth;
    unsigned int inner_timer;
    std::shared_ptr <BasicGeometry> geometries;
    double get_rand(double lo, double hi);
public:
    TreeNode(double p, double ha, double va, double cs,
             std::shared_ptr<BasicGeometry> g, const QVector3D &s, bool isLeaf_, int position);
    void plantNode(double res_vert, std::shared_ptr<BasicGeometry> lg);
    void drawMe(QGLShaderProgram *program, const QMatrix4x4 &projectionMatrix,
                const QMatrix4x4 &viewMatrix, const QMatrix4x4 &modelMatrix, int mode);
    void updateNode(double update_rate, double res_vert, std::shared_ptr<BasicGeometry> lg, int x, int drop);
    bool isLeaf;
    void setMaxBranches(int x) {
        max_branches = x;
        b_slots.resize(max_branches, false);
    }
    double get_growth() const {
        return growth;
    }
    int position;
};

#endif // TREENODE_H
