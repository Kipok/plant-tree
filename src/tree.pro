QT       += core gui widgets

TEMPLATE = app

SOURCES += main.cpp \
    tree.cpp \
    treenode.cpp \
    branchgeometry.cpp \
    leafgeometry.cpp \
    basicgeometry.cpp

qtHaveModule(opengl) {
    QT += opengl

    SOURCES += mainwidget.cpp

    HEADERS += \
        mainwidget.h

    RESOURCES += \
        shaders.qrc \
        textures.qrc
}

OTHER_FILES +=

HEADERS += \
    tree.h \
    treenode.h \
    branchgeometry.h \
    leafgeometry.h \
    basicgeometry.h
