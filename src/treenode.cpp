#include "treenode.h"

TreeNode::TreeNode(double p, double ha, double va, double cs,
                   std::shared_ptr<BasicGeometry> g, const QVector3D &s, bool isLeaf_, int pos)
    : pos(p), hor_angle(ha), vert_angle(va), cur_scale(cs),
      geometries(g), real_scale(s), scale(s), growth(0.0), inner_timer(0), isLeaf(isLeaf_), max_branches(10), position(pos)
{
    b_slots.resize(max_branches, false);
}

inline double TreeNode::get_rand(double lo, double hi)
{
    return lo + (1.0 * rand()) / (1.0 * RAND_MAX / (hi - lo));
}

void TreeNode::plantNode(double res_vert, std::shared_ptr<BasicGeometry> lg)
{
    if (this->isLeaf) {
        return;
    }
    int rand_pos = rand() % max_branches;
    if (b_slots[rand_pos]) {
        return;
    }
    b_slots[rand_pos] = true;
    double pos_min = min_height + rand_pos * (max_height - min_height) / max_branches;
    double pos_max = pos_min + (max_height - min_height) / max_branches;
    double p = get_rand(pos_min, pos_max);
    double cur_scale = get_rand(scale_min, scale_max);
    double ha = hor_min  + (rand() % (hor_range + 1)) * (hor_max - hor_min) / hor_range;
    double va = get_rand(vert_min - res_vert, vert_max - res_vert);

    if (scale.y() < min_length) {
        nodes.push_back(std::make_unique<TreeNode>(p, ha, va, cur_scale, lg, scale*cur_scale, true, rand_pos));
        return;
    }
    nodes.push_back(std::make_unique<TreeNode>(p, ha, va, cur_scale, geometries, scale*cur_scale, false, rand_pos));
}

void TreeNode::drawMe(QGLShaderProgram *program, const QMatrix4x4 &projectionMatrix,
                       const QMatrix4x4 &viewMatrix, const QMatrix4x4 &modelMatrix, int mode)
{
    QMatrix4x4 currentModel;
    if (mode) {
        currentModel.translate(scale / cur_scale * QVector3D(0.0f, pos, 0.0f));
    } else {
        currentModel.translate(real_scale / cur_scale * QVector3D(0.0f, pos, 0.0f));
    }
    currentModel.rotate(hor_angle, 0.0f, 1.0f, 0.0f);
    currentModel.rotate(vert_angle, 0.0f, 0.0f, -1.0f);

    QMatrix4x4 modelWithScale = currentModel;
    if (isLeaf) {
        modelWithScale.scale(growth * QVector3D(0.013f, 0.013f, 0.013f));
    } else {
        modelWithScale.scale(real_scale);
    }

    program->setUniformValue("mvp_matrix", projectionMatrix * viewMatrix * modelMatrix * modelWithScale);
    program->setUniformValue("mv_matrix", viewMatrix * modelMatrix * modelWithScale);
    program->setUniformValue("v_matrix", viewMatrix);
    program->setUniformValue("normalMatrix", (viewMatrix * modelMatrix * modelWithScale).normalMatrix());

    geometries->drawGeometry(program);

    for (int i = 0; i < nodes.size(); i++) {
        nodes[i]->drawMe(program, projectionMatrix, viewMatrix, modelMatrix * currentModel, mode);
    }
}

void TreeNode::updateNode(double update_rate, double res_vert, std::shared_ptr<BasicGeometry> lg, int x, int drop)
{
    if (inner_timer % x == 0 && update_rate != 0) {
        plantNode(res_vert, lg);
    }
    ++inner_timer;
    growth = std::min(growth + update_rate, 1.0);
    real_scale = scale * growth;
    for (int i = 0; i < nodes.size(); i++) {
        if (drop && nodes[i]->isLeaf) {
            double p = get_rand(0, 1);
            if (p > 0.9) {
                b_slots[nodes[i]->position] = false;
                nodes.erase(nodes.begin() + i);
            }
        }
        nodes[i]->updateNode(update_rate, res_vert + nodes[i]->vert_angle, lg, x, drop);
    }
}
